#!flask/bin/python
from flask import Flask, jsonify, abort, Response
from flask_cors import CORS
import os, sys
from flask import request
from flask_cors import CORS
import random, string, json
from requests.utils import quote
from os import listdir
from os.path import isfile, join
import requests
import json
import urllib3
import random, string
import subprocess

from datetime import datetime

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

PKM_API_HOST="pkm"

app = Flask(__name__)
CORS(app)
app.debug = True
app.reloader = True
cors = CORS(app, origins="*", methods=["GET", "POST", "PUT", "DELETE", "OPTIONS"], allow_headers=["Origin", "Content-Type", "Accept", "key"])


def doPOST(url, data, key=None):
	r = requests.post('http://' + PKM_API_HOST + ':8080' + url, headers=getHeaders(key), data = json.dumps(data, indent=4), verify=False)
	return r.status_code,r.text

def doPUT(url, data, key=None):
	r = requests.put('http://' + PKM_API_HOST + ':8080' + url, headers=getHeaders(key), data = json.dumps(data, indent=4), verify=False)
	return r.status_code,r.text

def doGET(url, key=None):
	r = requests.get('http://' + PKM_API_HOST + ':8080' + url, headers=getHeaders(key), verify=False)
	return r.status_code,r.text

def doDELETE(url, key=None):
	r = requests.delete('http://' + PKM_API_HOST + ':8080' + url, headers=getHeaders(key), verify=False)
	return r.status_code,r.text

def getHeaders(key=None):
	headers = {}
	headers["Content-Type"] = "application/json; charset=utf-8"
	headers["Accept"] = "application/json"
	headers["Access-Control-Allow-Origin"]= "*"
	headers["Access-Control-Allow-Headers"]= "*"
	headers["Access-Control-Allow-Methods"]= "*"

	if key != None:
		headers["key"] = key
	return headers

def createResponse(data, status=200):
	r=Response(response=json.dumps(data, indent=4), status=status)
	r.headers["Content-Type"] = "application/json; charset=utf-8"

	return r
def getRandomString(length):
	letters = string.ascii_lowercase
	result_str = ''.join(random.choice(letters) for i in range(length))
	return result_str

def loadInvocation(dbName, key, invocationID):
	url="/invocations/"+dbName+"/"+invocationID
	getStatus,document=doGET(url,key)
	if getStatus==200:
		document=json.loads(document)
		return document
	return None

def deleteInvocation(dbName, key, invocationID):
	url="/invocations/"+dbName+"/"+invocationID
	getStatus,document=doDELETE(url,key)
	return None

def loadAllInvocations(dbName, key, status, skip, limit):
	url="/invocations/"+dbName+"/"
	beggined=False
	if status!=None:
		beggined=True
		url=url+"?invocationStatus="+status
	
	if skip!=None:
		if beggined:
			url=url+"&"
		else:
			url=url+"?"
		url=url+"skip="+str(skip)
		beggined=True

	if limit!=None:
		if beggined:
			url=url+"&"
		else:
			url=url+"?"
		url=url+"limit="+str(limit)
		beggined=True


	getStatus,document=doGET(url,key)
	if getStatus==200:
		document=json.loads(document)
		return document
	return None

def loadToolsList(dbName, key, phases, tasks):
	#tasks: Requirements, SystemDesign, ArchitectureDesign, ModuleDesign, SystemImplementation, UnitTesting, IntegrationTesting, SystemTesting, AcceptanceTesting
	#phases: HLD, LLD, LLV, HLV
	url="/tools/"+dbName+"/"
	if phases==None and tasks==None:
		getStatus,document=doGET(url,key)
		if getStatus==200:
			document=json.loads(document)
			return document
		return None
	else:
		res=[]
		toolList=[]
		phasesList=[]
		if phases!=None:
			phasesList=phases.split(",")
		tasksList=[]
		if tasks!=None:
			tasksList=tasks.split(",")

		for phase in phasesList:
			getStatus,document=doGET(url+"?phase="+phase,key)
			if getStatus==200:
				document=json.loads(document)
				for tool in document:
					if not (tool["toolID"] in toolList):
						toolList.append(tool["toolID"])
						res.append(tool)

		for task in tasksList:
			getStatus,document=doGET(url+"?task="+task,key)
			if getStatus==200:
				document=json.loads(document)
				for tool in document:
					if not (tool["toolID"] in toolList):
						toolList.append(tool["toolID"])
						res.append(tool)
		return res

def loadTool(dbName, key, toolID):
	url="/tools/"+dbName+"/"+toolID
	getStatus,document=doGET(url,key)
	if getStatus==200:
		document=json.loads(document)
		return document
	return None

def saveNewInvocation(dbName, key, newInvocationData):
	status,document=doPUT("/invocations/"+dbName, newInvocationData, key)
	text_file = open("/processengine/test.txt", "w")
	n = text_file.write(str(status)+ "\n\n"+str(document))
	text_file.close()

def loadProcessStatus(dbName, key):
	doGetStatus, doGetText = doGET("/methodology/status/"+dbName, key)
	return json.loads(doGetText)

def updateProcessStatus(dbName,newProcessStatus, key):

	taskID=newProcessStatus['taskID']
	completed=newProcessStatus['completed']
	status=loadProcessStatus(dbName, key)

	for phase in status:
		phaseCompleted=True
		for task in phase['tasks']:
			if task['id']==taskID:
				task["completed"]=completed
			phaseCompleted=phaseCompleted and task["completed"]
		phase['completed']=phaseCompleted

		del phase['_id']

	doPutSt, doPutText = doPUT("/methodology/status/"+dbName, status, key)
	return status

@app.route('/decoder/pe/process/<dbName>/status', methods=['GET','POST'])
def processStatus(dbName):
	if not 'key' in request.headers:
		abort(400)
	key = request.headers.get('key')
	ps=None
	if request.method == 'POST':
		ps = updateProcessStatus(dbName, json.loads(request.data), key)

	elif request.method == 'GET':
		ps=loadProcessStatus(dbName, key)

	else:
		abort(405)

	if ps:
		return createResponse(ps)

@app.route('/decoder/pe/tools/<dbName>', methods=['GET', 'PUT'])
def manageToolsList(dbName):
	if not 'key' in request.headers:
		abort(400)
	key = request.headers.get('key')
	if request.method == 'GET':
		pP = request.args.get('processPhase')
		pT = request.args.get('processTask')

		loadedTools=loadToolsList(dbName, key, pP, pT)

		return createResponse(loadedTools)
	elif request.method == 'PUT':
		requestData=json.loads(request.data)
		putST, putText=doPUT("/tools/"+dbName+"/", [requestData], key)
		return createResponse(putText)


@app.route('/decoder/pe/tools/<dbName>/<toolID>', methods=['GET'])
def getToolInfo(dbName, toolID):
	if not 'key' in request.headers:
		abort(400)
	key = request.headers.get('key')

	tool=loadTool(dbName, key, toolID)
	if tool==None:
		abort(404)

	return createResponse(tool)


@app.route('/decoder/pe/invocations/<dbName>', methods=['GET','POST'])
def invocationsList(dbName):
	if not 'key' in request.headers:
		abort(400)
	key = request.headers.get('key')

	answer=[]
	if request.method == 'POST':
		invocationID=getRandomString(15)
		requestData=json.loads(request.data)
		tool=requestData['tool']
		invocationConfiguration=requestData['invocationConfiguration']
		user=requestData['user']
		timestampRequest=datetime.now().strftime("%Y%m%d_%H%M%S")
		invocationStatus="PENDING"
		newInvocationData=dict(invocationID=invocationID, tool=tool, user=user, timestampRequest=timestampRequest, invocationStatus=invocationStatus, invocationConfiguration=invocationConfiguration)

		saveNewInvocation(dbName, key, [newInvocationData])

		return createResponse(dict(invocationID=invocationID))

	elif request.method == 'GET':
		invocationStatus = request.args.get('invocationStatus') #PENDING | RUNING | COMPLETED
		skip = request.args.get('skip')
		limit = request.args.get('limit')
		return createResponse(loadAllInvocations(dbName, key, invocationStatus, skip, limit))


@app.route('/decoder/pe/invocations/<dbName>/<invocationID>', methods=['GET','PUT','DELETE','OPTIONS'])
def invocationInfo(dbName, invocationID):
	if not 'key' in request.headers:
		abort(400)
	key = request.headers.get('key')

	if request.method == 'OPTIONS':
		r=Response(response=json.dumps(data, indent=4), status=status)
		r.headers["Content-Type"] = "application/json; charset=utf-8"
		r.headers.add('Access-Control-Allow-Origin', '*')
		r.headers.add('Access-Control-Allow-Headers', '*')
		r.headers.add('Access-Control-Allow-Methods', 'GET, OPTIONS, PUT, DELETE')
		return r

	elif request.method == 'PUT':
		answer=dict()
		invocation=loadInvocation(dbName, key, invocationID)
		if invocation==None:
			abort(404)
		else:
			requestData=json.loads(request.data)
			if "invocationStatus" in requestData:
				invocation["invocationStatus"] = requestData["invocationStatus"].upper()
				if requestData["invocationStatus"].upper() == "RUNNING":
					invocation["timestampStart"] = datetime.now().strftime("%Y%m%d_%H%M%S")
				elif requestData["invocationStatus"].upper() == "COMPLETED" or requestData["invocationStatus"].upper() == "FAILED":
					if "message" in requestData:
						invocation["message"] = requestData["message"]
					invocation["timestampCompleted"] = datetime.now().strftime("%Y%m%d_%H%M%S")
			if "invocationResults" in requestData:
				invocation["invocationResults"] = requestData["invocationResults"]

			saveNewInvocation(dbName, key, [invocation])

			return createResponse(invocation)

	elif request.method == 'GET':
		invocation = loadInvocation(dbName, key, invocationID)
		if invocation==None:
			abort(404)
		else:
			return createResponse(invocation)

	elif request.method == 'DELETE':
		invocation=loadInvocation(dbName, key, invocationID)
		if invocation==None:
			abort(404)
		else:
			deleteInvocation(dbName, key, invocationID)
			return createResponse(invocation)

@app.after_request
def after_request(response):
	response.headers.add('Access-Control-Allow-Origin', '*')
	response.headers.add('Access-Control-Allow-Headers', '*')
	response.headers.add('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE')
	return response
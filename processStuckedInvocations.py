from datetime import datetime
import json
import string
import argparse
import requests
import time



PE_HOST = "localhost"

dateFormat = "%Y%m%d_%H%M%S"

MINUTES_TO_CONSIDER_STUCKED = 20

def doGET(url, key=None):
	r = requests.get(url, headers=getHeaders(key), verify=False)
	return r.status_code,r.text

def doPUT(url, data, key=None):
	r = requests.put(url, headers=getHeaders(key), data = data, verify=False)
	return r.status_code,r.text

def getHeaders(key=None):
	headers = {}
	headers["Content-Type"] = "application/json"
	headers["Accept"] = "application/json"
	headers["Access-Control-Allow-Origin"]= "*"
	headers["Access-Control-Allow-Headers"]= "*"
	headers["Access-Control-Allow-Methods"]= "*"
	if key != None:
		headers["key"] = key
	return headers

def doLogin(username, password):
	url = 'http://pkm-api_pkm_1:8080/user/login'
	data=dict(user_name=username, user_password=password)
	headers=dict(accept="application/json")
	headers["Content-Type"]="application/json"
	x = requests.post(url, data =json.dumps(data) , headers=headers)
	response=json.loads(x.text)
	return response['key']

"""
def getInvocationsByStatus(dbName, key, status):
	url="/decoder/pe/invocations/"+dbName+"?invocationStatus="+status
	
	status_code, text = doGET('http://' + PE_HOST + ':4000' + url, key)
	print(status_code, text)
	if status_code==200:
		document=json.loads(text)
		if document!=None:
			return document
	return []
"""


def getInvocationsByStatus(dbName, key, status):
	url="/decoder/pe/invocations/"+dbName
	
	status_code, text = doGET('http://' + PE_HOST + ':4000' + url, key)
	#print(status_code, text)
	res=[]
	if status_code==200:
		document=json.loads(text)
		if document!=None:
			for inv in document:
				if inv["invocationStatus"]==status:
					res.append(inv)
	return res

def getActiveProjects(key):
	url = 'http://pkm-api_pkm_1:8080/project'
	headers=dict(accept="application/json", key=key)
	headers["Content-Type"]="application/json"
	x = requests.get(url, headers=headers)
	response=json.loads(x.text)
	projects=[]
	for p in response:
		if "_id" in p:
			projects.append(p['name'])
	return projects


def getStuckedRunningInvocations(dbName, key):
	runingInvocations = getInvocationsByStatus(dbName, key, "RUNNING")
	res=[]
	for inv in runingInvocations:
		if "timestampStart" in inv:
			timeStarted=(datetime.now()-datetime.strptime(inv["timestampStart"], dateFormat)).total_seconds()/60
			print(timeStarted)
			if timeStarted>getMinutesToConsiderStucked(dbName, key, inv):
				res.append(inv)
		else:
			#setTimestampStart-->NOW
			url="/decoder/pe/invocations/"+dbName+"/"+inv["invocationID"]
			data=dict(timestampStart=datetime.now().strftime(dateFormat), message="timestampStart SETTED BY STUCKED INVOCATIONS PROCESSOR")
			status_code, text = doPUT('http://' + PE_HOST + ':4000' + url, json.dumps(data), key)
	if verbose:
		if len(res)==0:
			print("- PROJECT: "+dbName+" --> NO STUCKED INVOCATIONS")
		else:
			print("- PROJECT: "+dbName+" --> "+str(len(res))+" STUCKED INVOCATIONS")
	return res

def getMinutesToConsiderStucked(dbName, key, invocation):
	global toolsTimes
	
	if not(invocation['tool'] in toolsTimes):
		toolInfo=getToolInfo(dbName,invocation['tool'], key)
		if "minutesToStuck" in toolInfo:
			toolsTimes[invocation['tool']]=int(toolInfo["minutesToStuck"])
		else:
			try:
				if invocation['tool']=="javaparser":
					toolsTimes[invocation['tool']] = MINUTES_TO_CONSIDER_STUCKED*5
				else:
					toolsTimes[invocation['tool']] = MINUTES_TO_CONSIDER_STUCKED
				data = getToolInfo(dbName, invocation['tool'], key)
				data["minutesToStuck"]=toolsTimes[invocation['tool']]
				url="/decoder/pe/tools/"+dbName
				status_code, text = doPUT('http://' + PE_HOST + ':4000' + url, json.dumps(data), key)
				if verbose:
					print(dbName+" "+invocation['tool']+" TOOL --> setted Timeout to "+str(toolsTimes[invocation['tool']]))
			except Exception as e:
				if verbose:
					print(e.message)
	return toolsTimes[invocation['tool']]

def getToolInfo(dbName, toolID, key):
	url="/decoder/pe/tools/"+dbName+"/"+toolID
	
	status_code, text = doGET('http://' + PE_HOST + ':4000' + url, key)
	if status_code==200:
		document=json.loads(text)
		if document!=None:
			return document
	return []

def processStuckedInvocationsInProject(project, key):
	stuckedRunningInvocations = getStuckedRunningInvocations(project, key)
	for sri in stuckedRunningInvocations:
		setInvocationError(project, key, sri, message="Invocation Request Timeout")

def setInvocationError(dbName, key, invocation, message="Invocation Request Timeout"):
	url="/decoder/pe/invocations/"+dbName+"/"+invocation["invocationID"]
	data=dict(invocationStatus="FAILED", message=message)
	status_code, text = doPUT('http://' + PE_HOST + ':4000' + url, json.dumps(data), key)
	if verbose:
		timeStarted=(datetime.now()-datetime.strptime(invocation["timestampStart"], dateFormat)).total_seconds()/60
		print("- Invocation: "+str(invocation['invocationID'])+" [TOOL ID="+invocation['tool']+"] --> TIMED OUT ("+str(timeStarted).split(".")[0]+" minutes running)")


ap = argparse.ArgumentParser(description="Decoder Invocations Queue Manager",add_help=False)
ap.add_argument("-d", "--dbName", required=False, metavar="database name")
ap.add_argument("-u", "--username", required=False, metavar="user name", default="admin")
ap.add_argument("-p", "--password", required=False, metavar="user password", default="admin")
ap.add_argument("-k", "--key", required=False, metavar="login key", default=None)
ap.add_argument('-v','--verbose', required=False, help="Verbose Mode", action='store_true', default=False )

args = vars(ap.parse_args())

dbName=args['dbName']
username=args['username']
password=args['password']
key=args['key']
verbose=args['verbose']
toolsTimes=dict()

if key==None:
	key = doLogin(username, password)


if dbName:
	processStuckedInvocationsInProject(dbName, key)

else:
	toolsTimes=dict()
	projects=getActiveProjects(key)
	for project in sorted(projects):
		processStuckedInvocationsInProject(project, key)

		
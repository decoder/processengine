from datetime import datetime
import json
import string
import argparse
import requests
import time

import threading
import os
import sys
import subprocess

TIME_IN_SECONDS_BETWEEN_STUCK_PROCESSING=10
TIME_IN_SECONDS_BETWEEN_INVOCATIONS_PROCESSING=10

QUEUE_RUNNING=False
STUCKED_RUNNING=False

def doLogin(username, password):
    key = None
    while key is None:
        try:
            if verbose:
                print("trying to log in", flush=True)
            url = 'http://pkm-api_pkm_1:8080/user/login'
            data=dict(user_name=username, user_password=password)
            headers=dict(accept="application/json")
            headers["Content-Type"]="application/json"
            x = requests.post(url, data =json.dumps(data) , headers=headers)
            response=json.loads(x.text)
            key = response['key']
        except:
            if verbose:
                print("login failed (PKM not ready?): let retry later...", flush=True)
            time.sleep(1.0)
            continue
        break
    if verbose:
        print("successful login", flush=True)
    return key

def launchProcessStucked():
    global STUCKED_RUNNING
    if STUCKED_RUNNING:
        return
    STUCKED_RUNNING=True
    if verbose:
        print("-- LAUNCHING STUCKED INVOCATIONS PROCESSOR  --")
    order="/usr/bin/python3 /processengine/processStuckedInvocations.py -k "+key
    if verbose:
        order = order + " -v"
    if dbName:
        order = order + " -d "+dbName
    
    
    proc = subprocess.Popen(order,stdout=subprocess.PIPE, shell=True)
    (out, err) = proc.communicate()
    if verbose:
        print(out.decode('UTF-8'))
    STUCKED_RUNNING=False

def launchInvocationsQueueManager():
    global QUEUE_RUNNING
    if QUEUE_RUNNING:
        return
    QUEUE_RUNNING=True
    if verbose:
        print("-- LAUNCHING INVOCATIONS QUEUE MANAGER  --")
    order="/usr/bin/python3 /processengine/invocationsQueueManager.py -k "+key
    if verbose:
        order = order + " -v"
    if dbName:
        order = order + " -d "+dbName    
    proc = subprocess.Popen(order,stdout=subprocess.PIPE, shell=True)
    (out, err) = proc.communicate()
    if verbose:
        print(out.decode('UTF-8'))
    QUEUE_RUNNING=False

def set_interval(func, sec):
    def func_wrapper():
        set_interval(func, sec)
        func()
    t = threading.Timer(sec, func_wrapper)
    t.start()
    return t


ap = argparse.ArgumentParser(description="Decoder Invocations Queue Manager",add_help=False)
ap.add_argument("-d", "--dbName", required=False, metavar="database name")
ap.add_argument("-k", "--key", required=False, metavar="login key", default=None)
ap.add_argument('-v','--verbose', required=False, help="Verbose Mode", action='store_true', default=False )
args = vars(ap.parse_args())

config = json.load(open('/processengine/config.json'))
username=config['user']
password=config['pass']
intervalStucked=config['intervalStucked']
intervalQueue=config['intervalQueue']

dbName=args['dbName']
key=args['key']
verbose=args['verbose']

toolsTimes=dict()

if key==None:
    key = doLogin(username, password)

set_interval(launchProcessStucked, intervalStucked)
set_interval(launchInvocationsQueueManager, intervalQueue)
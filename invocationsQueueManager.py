from datetime import datetime
import json
import string
import argparse
import requests
import time


TIME_IN_SECONDS_BETWEEN_READS=5
TIME_IN_SECONDS_BETWEEN_INVOCATIONS=1
PE_HOST = "localhost"

dateFormat = "%Y%m%d_%H%M%S"

def doGET(url, key=None):
	r = requests.get(url, headers=getHeaders(key), verify=False)
	return r.status_code,r.text

def doPUT(url, data, key=None):
	r = requests.put(url, headers=getHeaders(key), data = data, verify=False)
	return r.status_code,r.text

def getHeaders(key=None):
	headers = {}
	headers["Content-Type"] = "application/json"
	headers["Accept"] = "application/json"
	headers["Access-Control-Allow-Origin"]= "*"
	headers["Access-Control-Allow-Headers"]= "*"
	headers["Access-Control-Allow-Methods"]= "*"
	if key != None:
		headers["key"] = key
	return headers

def getInvocationsByStatus(dbName, key, status):
	url="/decoder/pe/invocations/"+dbName+"?invocationStatus="+status

	status_code, text = doGET('http://' + PE_HOST + ':4000' + url, key)

	if status_code==200:
		document=json.loads(text)
		if document!=None:
			return document
	return []

def orderPendingInvocationsByDate(pendingInvocations):
	temp={}
	for pi in pendingInvocations:
		if pi["tool"] in temp:
			date1 = datetime.strptime(temp[pi['tool']]['timestampRequest'], dateFormat)
			date2 = datetime.strptime(pi['timestampRequest'], dateFormat)
			if date2<date1:
				temp[pi["tool"]] = pi
		else:
			temp[pi["tool"]] = pi
	res=[]
	for t in temp:
		res.append(temp[t])
	return res


def getPendingInvocationsReadyToBeLaunched(dbName, key):
	runingInvocations= getInvocationsByStatus(dbName, key, "RUNNING")
	orderedPendingInvocations=orderPendingInvocationsByDate(getInvocationsByStatus(dbName, key, "PENDING"))
	res=[]
	for inv in orderedPendingInvocations:
		if not isAnyInvocationForTool(runingInvocations+res, inv['tool']):
			toolInfo=getToolInfo(dbName,inv['tool'])
			if not ("localhost" in toolInfo["server"]):
				res.append(inv)
	return res


def isAnyInvocationForTool(invocations, toolID):
	for inv in invocations:
		if inv["tool"] == toolID:
			return True
	return False


def doLogin(username, password):
	url = 'http://pkm-api_pkm_1:8080/user/login'
	data=dict(user_name=username, user_password=password)
	headers=dict(accept="application/json")
	headers["Content-Type"]="application/json"
	x = requests.post(url, data =json.dumps(data) , headers=headers)
	response=json.loads(x.text)
	return response['key']


def setInvocationError(dbName, invocationID, message, key):
	url="/decoder/pe/invocations/"+dbName+"/"+invocationID
	data=dict(invocationStatus="FAILED", message=message)
	status_code, text = doPUT('http://' + PE_HOST + ':4000' + url, json.dumps(data), key)
	if verbose:
		print(status_code, text)
		print(message)

def getToolInfo(dbName, toolID):
	url="/decoder/pe/tools/"+dbName+"/"+toolID

	status_code, text = doGET('http://' + PE_HOST + ':4000' + url, key)
	if status_code==200:
		document=json.loads(text)
		if document!=None:
			return document
	return []

def processEndpointPath(endpointPath, invocationConfiguration, dbName):
	if "{" in endpointPath:
		processedEndpointPath = endpointPath
		for invConfKey in invocationConfiguration:
			if "{"+invConfKey+"}" in processedEndpointPath:
				processedEndpointPath = processedEndpointPath.replace("{"+invConfKey+"}",invocationConfiguration[invConfKey])
		if "{" in processedEndpointPath:
			return False, None

		return True, processedEndpointPath
	else:
		return True, endpointPath

def processQueryParameters(endpoint, invocationConfiguration, dbName):
	res=""
	for qp in endpoint["queryParameters"]:
		if 'isDBName' in qp and qp['isDBName']:
			res+="&"+qp['name']+"="+dbName
		else:
			if qp["required"]:
				if qp['name'] in invocationConfiguration:
					res+="&"+qp['name']+"="+invocationConfiguration[qp['name']]
				else:
					if "default" in qp:
						res+="&"+qp['name']+"="+str(qp['default'])
					else:
						return False, None
			else:
				if qp['name'] in invocationConfiguration:
					res+="&"+qp['name']+"="+invocationConfiguration[qp['name']]

	return True, res

def processRequestBody(endpoint, invocationConfiguration, dbName):
	res=dict()
	empty=True
	requestBodyProperties=endpoint["requestBody"]['properties']

	for prop in requestBodyProperties:

		if 'isDBName' in requestBodyProperties[prop] and requestBodyProperties[prop]['isDBName']:
			res[prop] = dbName
			empty=False

		elif "required" in requestBodyProperties[prop] and requestBodyProperties[prop]["required"]:
			if not prop in invocationConfiguration:
				return False, None
			else:
				res[prop] = invocationConfiguration[prop]
				empty=False
		elif ("required" in requestBodyProperties[prop] and (not requestBodyProperties[prop]["required"])):
			if prop in invocationConfiguration:
				res[prop] = invocationConfiguration[prop]
				empty=False

		elif not ("required" in requestBodyProperties[prop]):
			if prop in invocationConfiguration:
				res[prop] = invocationConfiguration[prop]
				empty=False

	if empty:
		return True, None
	return True, res


def launch(key, dbName, invocation):
	invocationID=invocation["invocationID"]
	toolInfo = getToolInfo(dbName, invocation['tool'])

	invocationConfiguration=invocation['invocationConfiguration']
	endpoint=toolInfo["endpoint"]
	endpointPath=endpoint["path"]
	endpointMethod=endpoint["method"]

	res=dict()
	res['dbName'] = dbName
	res['invocationID'] = invocationID
	res["tool"] = invocation['tool']
	res["server"] = toolInfo["server"]
	res['method'] = endpointMethod.upper()

	isEPPOk, processedEndpointPath = processEndpointPath(endpointPath, invocationConfiguration, dbName)
	if not isEPPOk:
		setInvocationError(dbName, invocationID, "ERROR Building End Point Path", key)
		if verbose:
			print(invocation)
		return None
	res['endpoint']=processedEndpointPath

	qp="?invocationID="+invocationID
	qp2=''
	if "queryParameters" in toolInfo["endpoint"]:
		isQPOk, qp2 = processQueryParameters(endpoint, invocationConfiguration, dbName)
		if not isQPOk:
			setInvocationError(dbName, invocationID, "ERROR Building Query Parameters", key)
			return None
	res['queryParameters']=qp+qp2

	if "requestBody" in toolInfo["endpoint"]:
		isRBOk, rb = processRequestBody(endpoint, invocationConfiguration, dbName)
		if not isRBOk:
			setInvocationError(dbName, invocationID, "ERROR Building Request Body", key)
			return None
		else:
			if not rb is None:
				res["requestBody"]=rb
	if verbose:
		print(json.dumps(res, indent=4))
	#ENVIAR A PEDRO
	sendInvocationRequestToPE(res, key)

def sendInvocationRequestToPE(inv, key):
	port=8081
	route="/process/tools/invocations"
	host="localhost"

	r = requests.post("http://"+host+":"+str(port)+route, headers=getHeaders(key), data = json.dumps(inv), verify=False)
	return r.status_code,r.text

def getActiveProjects(key):
	url = 'http://pkm-api_pkm_1:8080/project'
	headers=dict(accept="application/json", key=key)
	headers["Content-Type"]="application/json"
	x = requests.get(url, headers=headers)
	response=json.loads(x.text)
	projects=[]
	for p in response:
		if "_id" in p:
			projects.append(p['name'])
	return projects

def launchProjectInvocations(dbName, key):
	invToBeLaunched = getPendingInvocationsReadyToBeLaunched(dbName, key)
	if len(invToBeLaunched)>0:
		if verbose:
			print("-- LAUNCHING INVOCATIONS IN PROJECT "+dbName+" --")
		for inv in invToBeLaunched:
			launch(key, dbName, inv)
			time.sleep(TIME_IN_SECONDS_BETWEEN_INVOCATIONS)
	else:
		if verbose:
			print("-- NO LAUNCHABLE INVOCATIONS IN PROJECT "+dbName+" --")

ap = argparse.ArgumentParser(description="Decoder Invocations Queue Manager",add_help=False)
ap.add_argument("-d", "--dbName", required=False, metavar="database name")
ap.add_argument("-u", "--username", required=False, metavar="user name", default="admin")
ap.add_argument("-p", "--password", required=False, metavar="user password", default="admin")
ap.add_argument("-k", "--key", required=False, metavar="login key", default=None)
ap.add_argument('-v','--verbose', required=False, help="Verbose Mode", action='store_true', default=False )

args = vars(ap.parse_args())

dbName=args['dbName']
username=args['username']
password=args['password']
key=args['key']
verbose=args['verbose']

if key==None:
	key = doLogin(username, password)

if dbName:
	launchProjectInvocations(dbName, key)

else:
	projects=getActiveProjects(key)
	for project in sorted(projects):
		launchProjectInvocations(project, key)
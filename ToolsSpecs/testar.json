{
    "toolID": "testar",
    "toolName": "Testar",
    "phases": [
        "HLV"
    ],
    "tasks": [
        "SystemTesting"
    ],
    "openapi": {
        "openapi": "3.0.3",
        "info": {
            "version": "1.7.0",
            "title": "DECODER - TESTAR tool invocation API",
            "description": "An API that allows the integration of the TESTAR tool in the DECODER H2020 EU project to execute and generate Artefacts. This API will run on the TESTAR client machine.",
            "termsOfService": "",
            "contact": {
                "name": "TESTAR tool",
                "url": "https://testar.org/",
                "email": "fernando@testar.org"
            },
            "license": {
                "name": "BSD 3-Clause",
                "url": "https://github.com/TESTARtool/TESTAR_dev/blob/master/LICENSE"
            }
        },
        "servers": [{
            "url": "https://testar:8080"
        }],
        "paths": {
            "/testar": {
                "post": {
                    "tags": [
                        "TESTAR"
                    ],
                    "requestBody": {
                        "description": "Send the desired parameters to launch TESTAR through command line, connect with the SUT, generate the Artefacts and feed the PKM using the PKM-API with authentication.",
                        "required": true,
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$lookup": "#/components/schemas/settings"
                                }
                            }
                        }
                    },
                    "responses": {
                        "200": {
                            "description": "TESTAR was executed correctly, obtain the Artefacts Id.",
                            "content": {
                                "application/json": {
                                    "schema": {
                                        "$lookup": "#/components/schemas/testar-response"
                                    }
                                }
                            }
                        },
                        "400": {
                            "description": "Invalid TESTAR settings request",
                            "content": {
                                "application/json": {
                                    "schema": {
                                        "type": "string"
                                    }
                                }
                            }
                        },
                        "401": {
                            "description": "ERROR with OrientDB connection values or credentials",
                            "content": {
                                "application/json": {
                                    "schema": {
                                        "type": "string"
                                    }
                                }
                            }
                        },
                        "404": {
                            "description": "ERROR with the creation of the Artefacts",
                            "content": {
                                "application/json": {
                                    "schema": {
                                        "type": "string"
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "/testar/report/{artefactId}": {
                "get": {
                    "tags": [
                        "TESTAR"
                    ],
                    "description": "Visualize the HTML sequence report from the artefactID with the specific sequence number",
                    "parameters": [{
                        "name": "artefactId",
                        "in": "path",
                        "description": "Indicate the ArtefactId of the Test Results reports.",
                        "required": true,
                        "schema": {
                            "type": "string",
                            "example": "51b6eab8cd794eb62bb3e131"
                        }
                    }],
                    "responses": {
                        "200": {
                            "description": "TESTAR HTML report web server loaded. Visualize the reports using port 8091 (10.X.X.X:8091). Shutdown the server with the path /shutdown (10.X.X.X:8091/shutdown)."
                        },
                        "400": {
                            "description": "Invalid request"
                        },
                        "404": {
                            "description": "ERROR trying to find TESTAR HTML report"
                        }
                    }
                }
            },
            "/testar/analysis/{OrientDBname}": {
                "get": {
                    "tags": [
                        "TESTAR"
                    ],
                    "description": "Launch TESTAR Analysis mode to acces to the OrientDB database and visualize the State Model.",
                    "parameters": [{
                            "name": "OrientDBname",
                            "in": "path",
                            "description": "Indicate the OrientDB database name.",
                            "required": true,
                            "schema": {
                                "type": "string",
                                "example": "MyThaiStar"
                            }
                        },
                        {
                            "$lookup": "#/components/parameters/key"
                        }
                    ],
                    "responses": {
                        "200": {
                            "description": "TESTAR Analysis web server loaded, you can access using port 8090 and path /models. Shutdown the server with the path /shutdown."
                        },
                        "400": {
                            "description": "Invalid request"
                        },
                        "401": {
                            "description": "ERROR with OrientDB or PKM credentials"
                        },
                        "404": {
                            "description": "ERROR trying to load Analysis web server"
                        }
                    }
                }
            }
        },
        "components": {
            "parameters": {
                "key": {
                    "name": "key",
                    "description": "Access key to the PKM",
                    "in": "header",
                    "required": true,
                    "schema": {
                        "type": "string"
                    }
                }
            },
            "schemas": {
                "settings": {
                    "description": "TESTAR settings used to select a desired protocol, connect with the desired SUT and customize behaviour decisions.",
                    "properties": {
                        "sse": {
                            "description": "Select the TESTAR Java protocol to execute.",
                            "type": "string",
                            "example": "webdriver_MyThaiStar"
                        },
                        "ShowVisualSettingsDialogOnStartup": {
                            "description": "Enable or Disable TESTAR settings GUI. For remote API execution purposes this parameter should be always disables, in other case TESTAR will stay stucked with the GUI opened.",
                            "type": "boolean",
                            "enum": [
                                false
                            ],
                            "example": false
                        },
                        "Mode": {
                            "description": "Selects the TESTAR execution mode to Generate test sequences or Replay existing test sequences. Spy the SUT and Record user actions are not enabled in remote API automatic execution.",
                            "type": "string",
                            "enum": [
                                "Generate",
                                "Replay"
                            ],
                            "example": "Generate"
                        },
                        "AccessBridgeEnabled": {
                            "description": "Enable AccessBridge technology to create the widget tree for Java Swing applications.",
                            "type": "boolean",
                            "enum": [
                                true,
                                false
                            ],
                            "example": false
                        },
                        "SUTConnector": {
                            "description": "Indicate how to connect to the SUT. Launch executable file, attach to existing process, attach to existing windows through the Title or use Webdriver.",
                            "type": "string",
                            "enum": [
                                "COMMAND_LINE",
                                "SUT_PROCESS_NAME",
                                "SUT_WINDOW_TITLE",
                                "WEB_DRIVER"
                            ],
                            "example": "COMMAND_LINE"
                        },
                        "SUTConnectorValue": {
                            "description": "How to start the SUT through command line, the name of the pocess to connect, the title of the window to attach, or the path of the webdriver and url.",
                            "type": "string",
                            "example": "http://10.101.0.243:8081"
                        },
                        "Sequences": {
                            "description": "Number of sequences that TESTAR will start and close the session connections with the SUT.",
                            "type": "integer",
                            "example": 5,
                            "minimum": 1
                        },
                        "SequenceLength": {
                            "description": "Number of actions TESTAR will execute each sequence.",
                            "type": "integer",
                            "example": 100,
                            "minimum": 1
                        },
                        "SuspiciousTitles": {
                            "description": "Regular expression to be applied as Oracles to find suspicious and erroneous messages.",
                            "type": "string",
                            "example": ".*[eE]rror.*|.*[eE]xcepti[o?]n.*"
                        },
                        "TimeToFreeze": {
                            "description": "Time to check if the SUT do not respond to our interaction and consider it freezes.",
                            "type": "number",
                            "example": 30,
                            "minimum": 30,
                            "maximum": 120
                        },
                        "ProcessListenerEnabled": {
                            "description": "Enable reading the output and error buffer of the SUT to apply Oracles at process buffer level. (Only available for desktop applications through COMMAND_LINE).",
                            "type": "boolean",
                            "enum": [
                                true,
                                false
                            ],
                            "example": false
                        },
                        "SuspiciousProcessOutput": {
                            "description": "Regular expression to be applied as Oracles to find suspicious and erroneous messages at process buffer level. (Only available for desktop applications through COMMAND_LINE).",
                            "type": "string",
                            "example": ".*[eE]rror.*|.*[eE]xcepti[o?]n.*"
                        },
                        "ProcessLogs": {
                            "description": "Regular expression to be applied at process buffer level to save relevant information on TESTAR logs. (Only available for desktop applications through COMMAND_LINE).",
                            "type": "string",
                            "example": ".*[eE]rror.*|.*[eE]xcepti[o?]n.*"
                        },
                        "ClickFilter": {
                            "description": "Regular expression to filter widgets we don not want to interact with.",
                            "type": "string",
                            "example": ".*[sS]istema.*|.*[sS]ystem.*|.*[cC]errar.*|.*[cC]lose.*|.*[sS]alir.*|.*[eE]xit.*|.*[mM]inimizar.*|.*[mM]inimi[zs]e.*|.*[sS]ave.*|.*[gG]uardar.*|.*[fF]ormat.*|.*[fF]ormatear.*|.*[pP]rint.*|.*[iI]mprimir.*|.*[eE]mail.*|.*[fF]ile.*|.*[aA]rchivo.*|.*[dD]isconnect.*|.*[dD]esconectar.*"
                        },
                        "ForceForeground": {
                            "description": "Force the SUT to be in the foreground if TESTAR detects that is in the background.",
                            "type": "boolean",
                            "enum": [
                                true,
                                false
                            ],
                            "example": true
                        },
                        "ActionDuration": {
                            "description": "How fast TESTAR will move the mouse or press the keyboard to interact with the SUT (seconds).",
                            "type": "number",
                            "example": 0.3,
                            "minimum": 0.1
                        },
                        "TimeToWaitAfterAction": {
                            "description": "Time that TESTAR will wait after the execution of an action before continue his flow generation (seconds).",
                            "type": "number",
                            "example": 0.3,
                            "minimum": 0.1
                        },
                        "MaxTime": {
                            "description": "Maximum time that a TESTAR sequence can take. After this time not more action will be executed (seconds).",
                            "type": "number",
                            "example": 31536000,
                            "minimum": 600
                        },
                        "StartupTime": {
                            "description": "Maximum time that TESTAR waits for the SUT to load (seconds). Once the SUT UI is ready, TESTAR will start the test sequence.",
                            "type": "number",
                            "example": 5,
                            "minimum": 2
                        },
                        "MaxReward": {
                            "description": "Maximum Reward for Reinforecement Learning action selection algorithms.",
                            "type": "number",
                            "example": 99999
                        },
                        "Discount": {
                            "description": "Discount Reward to be applied in Reinforcement Learning action selection algorithms.",
                            "type": "number",
                            "example": 0.95
                        },
                        "AlwaysCompile": {
                            "description": "Compiles always the selected Java protocol before start the TESTAR execution.",
                            "type": "boolean",
                            "enum": [
                                true,
                                false
                            ],
                            "example": true
                        },
                        "OnlySaveFaultySequences": {
                            "description": "Save only TESTAR sequence that contains fails or warnings.",
                            "type": "boolean",
                            "enum": [
                                true,
                                false
                            ],
                            "example": false
                        },
                        "StopGenerationOnFault": {
                            "description": "Stop the TESTAR sequence if an error is detected or continue until all SequenceLength actions are executed.",
                            "type": "boolean",
                            "enum": [
                                true,
                                false
                            ],
                            "example": false
                        },
                        "ProcessesToKillDuringTest": {
                            "description": "Processes to kill during TESTAR sequences execution. In case some SUT launch an external an undesired process.",
                            "type": "string",
                            "example": ""
                        },
                        "PathToReplaySequence": {
                            "description": "Path to a directory that contains a sequence generated by TESTAR previously, to repeat same actions.",
                            "type": "string",
                            "example": "C:\\\\Users\\\\testar\\\\output\\\\sequences\\\\sequence1.testar"
                        },
                        "OutputDir": {
                            "description": "Select the system directory to save TESTAR output results.",
                            "type": "string",
                            "example": "output"
                        },
                        "TempDir": {
                            "description": "Select the system directory to save files temporally.",
                            "type": "string",
                            "example": "output/temp"
                        },
                        "MyClassPath": {
                            "description": "Indicate the settings directory that contains TESTAR compiled java class protocols.",
                            "type": "string",
                            "example": "settings"
                        },
                        "OverrideWebDriverDisplayScale": {
                            "description": "Sometimes system screen display options affects action coordinates. Overrides the displayscale obtained from the system to solve issues with actions coordinates.",
                            "type": "number",
                            "example": 1
                        },
                        "StateModelEnabled": {
                            "description": "Enable the use of TESTAR State Model functionality.",
                            "type": "boolean",
                            "enum": [
                                true,
                                false
                            ],
                            "example": true
                        },
                        "DataStore": {
                            "description": "Indicate the graph database where are going to use.",
                            "type": "string",
                            "enum": [
                                "OrientDB"
                            ],
                            "example": "OrientDB"
                        },
                        "DataStoreType": {
                            "description": "Indicate the type of connection with the graph database. Remote or local.",
                            "type": "string",
                            "enum": [
                                "remote",
                                "plocal"
                            ],
                            "example": "remote"
                        },
                        "DataStoreServer": {
                            "description": "Indicate the remote address of the graph database.",
                            "type": "string",
                            "example": "10.101.0.100"
                        },
                        "DataStoreDirectory": {
                            "description": "Indicate the directory that contains the plocal graph database.",
                            "type": "string",
                            "example": "C:\\\\Users\\\\testar\\\\Desktop\\\\orientdb-3.0.34\\\\databases"
                        },
                        "DataStoreDB": {
                            "description": "The name of the database to generate the TESTAR State Model.",
                            "type": "string",
                            "example": "MyThaiStar"
                        },
                        "DataStoreUser": {
                            "description": "Existing User in the graph database with admin permissions.",
                            "type": "string",
                            "example": "testar"
                        },
                        "DataStorePassword": {
                            "description": "Password credentials that uses the previous User.",
                            "type": "string",
                            "example": "testar"
                        },
                        "DataStoreMode": {
                            "description": "Select how the data will be stored in the database.",
                            "type": "string",
                            "enum": [
                                "instant",
                                "delayed",
                                "hybrid",
                                "none"
                            ],
                            "example": "instant"
                        },
                        "ResetDataStore": {
                            "description": "WARNING, This DROP all existing content in the database before create the new TESTAR State Model with the new sequence.",
                            "type": "boolean",
                            "enum": [
                                true,
                                false
                            ],
                            "example": false
                        },
                        "ActionSelectionAlgorithm": {
                            "description": "State Model action selection algorithm. If model is not deterministic TESTAR will alternate to random selection.",
                            "type": "string",
                            "enum": [
                                "unvisited",
                                "random"
                            ],
                            "example": "unvisited"
                        },
                        "StateModelStoreWidgets": {
                            "description": "Indicate if we want to store the widget tree everytime a new Concrete State is discovered (Higher memory consumption, slower sequences).",
                            "type": "boolean",
                            "enum": [
                                true,
                                false
                            ],
                            "example": false
                        },
                        "AbstractStateAttributes": {
                            "description": "Specify the widget attributes that you want to use in constructing the widget and state abstract hash strings. Use a comma separated list.",
                            "type": "string",
                            "example": "WebWidgetIsOffScreen,WebWidgetHref,WebWidgetTagName,WebWidgetTextContent"
                        },
                        "ApplicationName": {
                            "description": "Use a name identifier to represent the current application. This will be used to create the output structure reports and for State Model purposes.",
                            "type": "string",
                            "example": "MyThaiStar"
                        },
                        "ApplicationVersion": {
                            "description": "Use a version identifier to represent the current application. This will be used to create the output structure reports and for State Model purposes.",
                            "type": "number",
                            "example": 1.2
                        },
                        "PreviousApplicationName": {
                            "description": "Name identifier that indicates which one is the previous name of the application that acts as SUT. This will be used for State Model Difference purposes.",
                            "type": "string",
                            "example": "MyThaiStar"
                        },
                        "PreviousApplicationVersion": {
                            "description": "Version identifier that indicates which one is the previous version of the application that acts as SUT. This will be used for State Model Difference purposes.",
                            "type": "number",
                            "example": 1.1
                        },
                        "StateModelDifferenceAutomaticReport": {
                            "description": "Try to automatically create the State Model Difference report at the end of the current TESTAR execution.",
                            "type": "boolean",
                            "enum": [
                                true,
                                false
                            ],
                            "example": true
                        },
                        "PKMaddress": {
                            "description": "IP address of the server that contains the PKM",
                            "type": "string",
                            "example": "10.100.0.200"
                        },
                        "PKMport": {
                            "description": "Port of the PKM-API",
                            "type": "string",
                            "example": "8080"
                        },
                        "PKMdatabase": {
                            "description": "PKM project to store TESTAR Artefacts",
                            "type": "string",
                            "example": "myproject"
                        },
                        "PKMusername": {
                            "description": "User with permissions to add Artefacts in the PKM",
                            "type": "string",
                            "example": "garfield"
                        },
                        "PKMkey": {
                            "description": "Key or password for authentication",
                            "type": "string",
                            "example": "5cCI6IkpXVCJ9.eyJ1c2VyX25hbWU"
                        }
                    }
                },
                "testar-response": {
                    "description": "The response of the TESTAR tool execution.",
                    "properties": {
                        "artefactId": {
                            "description": "The IDs of the Artefacts generated by the execution of TESTAR tool and inserted into the PKM. Currently TESTAR is going to create two Artefacts every execution, Test Results and State Model.",
                            "type": "array",
                            "items": {
                                "type": "string"
                            },
                            "minItems": 2,
                            "maxItems": 2,
                            "example": [
                                "TestResults ArtefactId : 51b6eab8cd794eb62bb3e131",
                                "StateModel ArtefactId : 51b6eab8cd794eb62bb3e132"
                            ]
                        }
                    }
                }
            }
        }
    }
}
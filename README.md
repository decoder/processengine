# PROCESS ENGINE API
This repository contains the Process Engine API

**PORT: 4000**

## ENDPOINTS
> Note: All Endpoints require a `key` header with the PKM User Login Key.
### Process Status:
```sh
/decoder/pe/process/<dbName>/status
```
#### _Methods_
| Method | Description |
| ------ | ------ |
| **<code>GET</code>** | Retrieve the Process Current Status for the specified project|
| **<code>PUT</code>** | Updates the Process Current Status for the specified project. A JSON is expected with the information to update |

### _JSON_ (PUT)
The JSON to set the **Process Current Status** should be like:

```sh
[
    {
        "phaseNumber":1,
        "name":"HLD",
        "id": "hld",
        "description":"High Level Design",
        "completed":true,
        "tasks":[
            {
                "taskNumber":1,
                "name":"Requirements",
                "id": "requirements",
                "completed":true
            },
            {
                "taskNumber":2,
                "name":"System Design",
                "id": "systemDesign",
                "completed":true
            },
            {
                "taskNumber":3,
                "name":"Architecture Design",
                "id": "architectureDesign",
                "completed":true
            }
        ]
    },
    {
        "phaseNumber":2,
        "name":"LLD",
        "id": "lld",
        "description":"Low Level Design",
        "completed":true,
        "tasks":[
            {
                "taskNumber":1,
                "name":"Module Design",
                "id": "moduleDesign",
                "completed":true
            },
            {
                "taskNumber":2,
                "name":"System Implementation",
                "id":"systemImplementation",
                "completed":true
            }
        ]
    },
    {
        "phaseNumber":3,
        "name":"LLV",
        "id": "llv",
        "description":"Low Level Verification",
        "completed":true,
        "tasks":[
            {
                "taskNumber":1,
                "name":"Unit Testing",
                "id":"unitTesting",
                "completed":true
            },
            {
                "taskNumber":2,
                "name":"Integration Testing",
                "id":"integrationTesting",
                "completed":true
            }
        ]
    },
    {
        "phaseNumber":4,
        "name":"HLV",
        "id": "hlv",
        "description":"High Level Verification",
        "completed":true,
        "tasks":[
            {
                "taskNumber":1,
                "name":"System Testing",
                "id":"systemTesting",
                "completed":true
            },
            {
                "taskNumber":2,
                "name":"Acceptance Testing",
                "id":"acceptanceTesting",
                "completed":true
            }
        ]
    }
]
```


### Invocations List:
```sh
/decoder/pe/invocations/<dbName>?invocationStatus=*
```
#### _Methods_
| Method | Description |
| ------ | ------ |
| **<code>GET</code>** | Retrieve the invocations list. <br/>Accepts an optional query parameter <code>invocationStatus</code> to filter by the status of the invocation (<code>PENDING \| RUNNING \| COMPLETED \| FAILED</code>)   |
| **<code>POST</code>** | Creates a new invocation on the PKM   |

### _JSON_ (POST)
The JSON to create a new **invocation** should be like:

```sh
{
    "tool":"javaParser",
    "user":"jsmith",
    "invocationConfiguration":{
        "generate":"all",
        "dbName":"myThaiStar",
        "sourceFileName":"code/rawsourcecode/mythaistar/java/mtsj/api/src/main/java/com/devonfw/application/mtsj/bookingmanagement/service/api/rest/BookingmanagementRestService.java"
    }
}
```

If everything is OK, the answer will be a **200 OK** and the body will contain the new invocation ID:

```sh
{
    "invocationID":"14139asdaklj123"
}
```

### Invocation:
```sh
/decoder/pe/invocations/<dbName>/<invocationID>
```
#### _Methods_
| Method | Description |
| ------ | ------ |
| **<code>GET</code>** | Retrieves the invocation specified by the <code>invocationID</code>|
| **<code>PUT</code>** | Updates the invocation <code>invocationID</code> with the specified information. A JSON is expected with the information to update |

### _JSON_ (PUT)
The JSON to set the **invocation as completed** *(or failed)* should be like:

```sh
{
    "invocationStatus":"COMPLETED",
    "invocationResults":[
        {
            "path":"/code/java/sourcecode/mythaistar/java/mtsj/core/src/main/java/com/devonfw/application/mtsj/usermanagement/logic/impl/UsermanagementImpl.java"
        },
        {
            "path":"/code/java/comments/mythaistar/java/mtsj/core/src/main/java/com/devonfw/application/mtsj/usermanagement/logic/impl/UsermanagementImpl.java"
        },
        {
            "path":"/code/java/annotations/mythaistar/java/mtsj/core/src/main/java/com/devonfw/application/mtsj/usermanagement/logic/impl/UsermanagementImpl.java"
        }
    ]
}
```

### Tools List:
```sh
/decoder/pe/tools/<dbName>?processTask=*&processPhase=*
```
#### _Methods_
| Method | Description |
| ------ | ------ |
| **<code>GET</code>** | Retrieves the tools list. <br/>Accepts two optional query parameter <code>processPhase</code> and <code>processTask</code> to filter by the phases or the tasks (separated by commas) of the process where the tools are available   |

### Tool:
```sh
/decoder/pe/tools/<dbName>/<toolName>
```
#### _Methods_
| Method | Description |
| ------ | ------ |
| **<code>GET</code>** | Retrieves the tool by the specified name.|




#!/bin/bash
VOLUME_CONFIG=/processengine/volume_config
[ -f "${VOLUME_CONFIG}/config.json" ] && \
    cp -f "${VOLUME_CONFIG}/config.json" /processengine

alias python=python3


cd /processengine
java -jar decoder-process-server-0.6.jar&
python3 queueTimersManager.py&
sh runRestService.sh -p 4000
#!/bin/bash
alias python=python3

service mysql stop
usermod -d /var/lib/mysql/ mysql
service mysql start

MAINDB='decoder'

mysql -uroot -e "CREATE DATABASE $MAINDB /*\!40100 DEFAULT CHARACTER SET utf8 */;"
mysql -uroot -e "CREATE USER 'root'@'%'"
mysql -uroot -e "GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' WITH GRANT OPTION;"
mysql -uroot -e "FLUSH PRIVILEGES;"
mysql -uroot -e "UPDATE mysql.user SET plugin='mysql_native_password' WHERE User='root'"
mysql -uroot -e "FLUSH PRIVILEGES;"

cd /processengine
java -jar decoder-process-server-1.0.jar&
sh runRestService.sh -p 4000